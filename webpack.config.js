const path = require('path');

module.exports = {
    entry: './src/history.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'js')
    }
};
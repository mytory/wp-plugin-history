# 워드프레스 연혁 플러그인

`composer.json` 예시

~~~ json
{
  "name": "my/project",
  "type": "project",
  "authors": [
    {
      "name": "an, hyeong-woo",
      "email": "mytory@gmail.com"
    }
  ],
  "repositories": [
    {
      "type": "git",
      "url": "git@bitbucket.org:mytory/wp-plugin-history.git"
    }
  ],
  "require": {
    "mytory/wp-plugin-history": "^0.1.1"
  }
}
~~~

## version history

### 0.3.0

연도를 두자리수로 적은 경우 현재 연도와 비교해 1900년대인지 2000년대인지 감지한다. 
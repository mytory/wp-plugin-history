<?php

namespace Mytory\WpPluginHistory;

use WP_Query;

class InitBatch {
	public function __construct() {

		add_action( 'admin_menu', [ $this, 'subMenu' ] );

	}

	function subMenu() {
		add_submenu_page( 'mytory_history', '연혁 일괄 입력', '일괄 입력', 'edit_others_posts', 'mytory_history_batch_insert',
			[ $this, 'inner' ] );
	}


	function inner() {
		global $wpdb;

		include __DIR__ . '/helper.php';

		if ( $_SERVER['REQUEST_METHOD'] === 'GET' ) {
			include __DIR__ . '/html/history-batch.php';
		}

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			$content   = "날짜\t내용\n" . $_POST['content'];
			$temp_data = csv_to_array( $content, "\t" );
			$data      = [];
			foreach ( $temp_data as $k => $v ) {
				if ( empty( $v['내용'] ) ) {
					continue;
				}
				if ( empty( $v['날짜'] ) and ! empty( $v['내용'] ) and count( $data ) > 0 ) {
					// 내용만 있고 날짜가 없고, 앞서 입력한 데이터가 있다면 앞의 날짜에 붙인다. 줄바꿈한 걸로 보는 것.
					$data[ count( $data ) - 1 ]['내용'] .= PHP_EOL . $v['내용'];
				}

				if ( $v['날짜'] and $v['내용'] ) {
					$data[] = $v;
				}
			}

			usort( $data, function ( $a, $b ) {
				// 시간 역순 정렬
				return $a['날짜'] > $b['날짜'] ? - 1 : 1;
			} );

			$the_query = new WP_Query( [
				'post_type'      => 'mytory_history',
				'posts_per_page' => 1,
				'post_status'    => 'any',
				'orderby'        => 'menu_order',
				'order'          => 'DESC',
			] );

			if ( $the_query->post_count > 0 ) {
				$menu_order = $the_query->post->menu_order + 1;
			} else {
				$menu_order = 1;
			}

			$result = [
				'inserted_count' => 0,
				'error_count'    => 0,
				'errors'         => [],
			];
			foreach ( $data as $item ) {
				$post_id = wp_insert_post( [
					'post_type'    => 'mytory_history',
					'post_title'   => $item['날짜'],
					'post_content' => $item['내용'],
					'menu_order'   => $menu_order ++,
					'post_status'  => 'private',
				], true );

				if ( is_wp_error( $post_id ) ) {
					$result['error_count'] ++;
					$wp_error           = $post_id;
					$result['errors'][] = [
						'code'    => $wp_error->get_error_code(),
						'message' => $wp_error->get_error_message(),
					];
				} else {
					$result['inserted_count'] ++;
				}
			}

			include __DIR__ . '/html/history-batch-result.php';

		}
	}

}
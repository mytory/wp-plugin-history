import Vue from 'vue/dist/vue';
import draggable from 'vuedraggable';
import axios from 'axios';
import Swal from 'sweetalert2';

Vue.component('draggable', draggable);

new Vue({
    el: '.js-vue',
    data: {
        historyItems: historyItems,
        removedItems: [],
        dateDelimiter: dateDelimiter,
        isProcess: false
    },
    methods: {
        addRow() {
            this.historyItems.unshift({
                ID: null,
                content: '',
                date: '',
                url: ''
            });
        },
        remove(index) {
            if (this.historyItems[index].ID) {
                this.removedItems.push(this.historyItems[index]);
            }
            this.historyItems.splice(index, 1);
        },
        save() {
            this.isProcess = true;
            const params = new URLSearchParams();
            axios.post(ajaxurl + '?action=mytory_history', {
                history_items: this.historyItems,
                removed_items: this.removedItems,
                date_delimiter: this.dateDelimiter,
            })
                .then(res => {
                    this.isProcess = false;
                    if (res.data.result === 'error') {
                        throw res.data.message;
                    } else {
                        Swal.fire({
                            title: res.data.message,
                            icon: 'success',
                        });
                    }
                })
                .catch(error => {
                    this.isProcess = false;
                    Swal.fire({
                        title: 'Error',
                        text: error,
                        icon: 'error'
                    })
                });
        },
        twoPlaceYearToFourPlace(date) {
            // 현재 연도 뒷자리 숫자 두 개
            const twoPlaceCurrentYear = (new Date()).getFullYear().toString().substr(2, 2);
            let year = date.split(this.dateDelimiter)[0];
            if (year.length === 2) {
                if (year > twoPlaceCurrentYear) {
                    // 두자릿수의 현재 연도보다 연혁의 두자릿수 연도가 크면 1900년대 연혁
                    date = '19' + date;
                } else {
                    date = '20' + date;
                }
            }
            return date;
        },
        sortAsc() {
            this.historyItems.sort((a, b) => {
                const dateA = this.getDateObjectFromDate(a.date);
                const dateB = this.getDateObjectFromDate(b.date);
                return dateA < dateB ? -1 : 1;
            });
            alert('정렬을 완료했습니다.');
        },
        sortDesc() {
            this.historyItems.sort((a, b) => {
                const dateA = this.getDateObjectFromDate(a.date);
                const dateB = this.getDateObjectFromDate(b.date);
                return dateA > dateB ? -1 : 1;
            });
            alert('정렬을 완료했습니다.');
        },
        getDateObjectFromDate(dateString) {
            const dateArray = this.twoPlaceYearToFourPlace(dateString).toString().split(this.dateDelimiter);
            const year = dateArray[0];
            const month = (dateArray[1].length === 2) ? dateArray[1] : `0${dateArray[1]}`;
            const day = (dateArray[2].length === 2) ? dateArray[2] : `0${dateArray[2]}`;
            return new Date(`${year}-${month}-${day}`);
        }
    }
});

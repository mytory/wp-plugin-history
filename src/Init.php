<?php

namespace Mytory\WpPluginHistory;


class Init {
	public function __construct() {
		add_action( 'init', [ $this, 'registerPostType' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueueScripts' ] );
		add_action( 'admin_menu', [ $this, 'adminMenu' ] );
		add_action( 'wp_ajax_mytory_history', [ $this, 'save' ]);
		new InitBatch();
	}

	public function registerPostType() {
		// 연혁
		$args = [
			'label' => '연혁',
		];

		register_post_type( 'mytory_history', $args );
	}

	public function enqueueScripts() {
		global $current_screen;
		if (!empty($current_screen->id) && $current_screen->id == 'toplevel_page_mytory_history') {
			wp_enqueue_script('sortable', 'https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js');
			wp_enqueue_script('vue-draggable', 'https://cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js', ['vue', 'sortable']);
		}
	}

	public function adminMenu() {
		add_menu_page( '연혁 편집', '연혁 편집', 'edit_others_posts', 'mytory_history', [ $this, 'html' ], 'dashicons-hourglass', 50 );
	}

	public function html() {
		global $current_screen, $wp_query, $post;
		include 'html.php';
	}

	public function save() {
	    try {
		    $data = json_decode(file_get_contents('php://input'));

		    update_option( 'mytory_history_date_delimiter', $data->date_delimiter );

		    foreach ( $data->history_items as $i => $item ) {
			    $postarr = [
				    'ID'           => $item->ID,
				    'post_type'    => 'mytory_history',
				    'post_title'   => $item->date,
				    'post_content' => $item->content,
				    'menu_order'   => $i,
				    'post_status'  => 'public',
			    ];

			    if ($item->ID) {
				    $post_id = wp_update_post($postarr, true);
			    } else {
				    $post_id = wp_insert_post($postarr, true);
			    }

			    if (is_wp_error($post_id)) {
				    $wp_error = $post_id;
				    throw new \Exception($wp_error->get_error_message());
			    }

			    update_post_meta($post_id, '연혁url', $item->url);
		    }

		    foreach ( $data->removed_items as $item ) {
                wp_delete_post( $item->ID );
		    }

		    echo json_encode([
		            'result' => 'success',
                    'message' => '저장했습니다.'
            ]);

        } catch (\Exception $e) {
	        echo json_encode([
	            'result' => 'error',
                'message' => $e->getMessage(),
            ]);
        }
		die();
	}
}

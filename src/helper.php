<?php
/**
 * Convert a comma separated file into an associated array.
 * The first row should contain the array keys.
 *
 * Example:
 *
 * @param $content
 * @param string $delimiter The separator used in the file
 *
 * @return array
 * @link http://gist.github.com/385876
 * @author Jay Williams <http://myd3.com/>
 * @copyright Copyright (c) 2010, Jay Williams
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */
function csv_to_array( $content, $delimiter = ',' ) {
	if ( empty( $content ) ) {
		return [];
	}

	$header = null;
	$data   = array();
	$lines  = explode( PHP_EOL, $content );
	foreach ( $lines as $line ) {
		$row = str_getcsv( $line, $delimiter, '"' );
		if ( ! $header ) {
			$header = $row;
		} else {

			if ( count( $row ) < count( $header ) ) {
				while ( count( $row ) < count( $header ) ) {
					$row[] = '';
				}
			}

			if ( count( $row ) > count( $header ) ) {
				while ( count( $row ) > count( $header ) ) {
					array_pop( $row );
				}
			}

			$data[] = array_combine( $header, $row );
		}
	}

	return $data;
}
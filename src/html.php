<?php
global $wp_query, $post;
$wp_query = new WP_Query(array(
    'post_type'      => 'mytory_history',
    'posts_per_page' => -1,
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'post_status'    => 'any',
));
?>
<style>
    .container {
        background-color: #fff;
        padding: .5em;
    }

    .row {
        display: flex;
        padding: .5em 0;
    }

    .move-icon {
        opacity: 0;
    }

    .row:hover .move-icon {
        opacity: 1;
        cursor: move;
    }

    .row .content {
        flex-grow: 1;
    }

    .wp-core-ui .button-link {
        text-decoration: none;
    }

    .button-link {
        -webkit-transition-property: border, background, color;
        transition-property: border, background, color;
        -webkit-transition-duration: .05s;
        transition-duration: .05s;
        -webkit-transition-timing-function: ease-in-out;
        transition-timing-function: ease-in-out;
        color: #0073aa;
    }

    .button-link:hover,
    .button-link:active {
        color: #00a0d2;
    }

    .translucence {
        opacity: 0.5;
    }
</style>
<div class="wrap  js-vue">
    <h1>연혁 편집</h1>

    <div class="card">
        드래그 앤 드롭으로 순서를 조정할 수 있습니다.
    </div>
    <form :class="isProcess ? 'translucence' : ''" method="post" :disabled="isProcess" @submit.prevent="save">
        <?php wp_nonce_field('save_mytory_history', 'mytory_history_nonce') ?>
        <p>
            <input type="submit" name="submit" id="submit" class="button  button-primary"
                   :value="isProcess ? '저장중...' : '변경 사항 저장'" :disabled="isProcess">
            <button type="button" class="button  button-secondary" @click="addRow">행 추가</button>
            <button type="button" class="button  button-secondary" @click="sortDesc">내림차순 정렬</button>
            <button type="button" class="button  button-secondary" @click="sortAsc">오름차순 정렬</button>
        </p>
        <p>
            날짜 구분 기호:
            <input type="text" v-model="dateDelimiter" name="date_delimiter"
                   title="날짜 구분 기호">
            <span class="description">예) 16.09.20 형태로 날짜를 표기한다면 구분 기호는 <code>.</code>입니다.</span>
        </p>

        <div class="container">
            <div class="row" style="border-bottom: 1px solid #ccc;">
                <div style="width: 45px; text-align: center;"></div>
                <div style="width: 140px;">날짜</div>
                <div class="content">내용</div>
                <div style="text-align: center; width: 45px;"></div>
            </div>
            <draggable v-model="historyItems" ghost-class="u-translucence">
                <div class="row" v-for="(item, index) in historyItems" :key="item.ID">
                    <div style="width: 45px; text-align: center;">
                        <i class="dashicons  dashicons-move  move-icon"></i>
                    </div>
                    <div style="width: 140px;">
                        <input class="large-text" type="text" v-model="item.date" title="날짜">
                    </div>
                    <div class="content">
                        <textarea class="large-text" v-model="item.content" title="내용"></textarea>
                        <input type="text" class="large-text" v-model="item.url"
                               placeholder="관련 링크. 예) https://example.com">
                    </div>
                    <div style="text-align: center; width: 45px;">
                        <button type="button" class="button-link" title="삭제" @click="remove(index)">
                            <i class="dashicons  dashicons-no"></i>
                        </button>
                    </div>
                </div>
            </draggable>
        </div>

        <input type="submit" name="submit" id="submit" class="button  button-primary"
               :value="isProcess ? '저장중...' : '변경 사항 저장'" :disabled="isProcess">
    </form>
</div>

<?php
$history_items = [];
while (have_posts()): the_post();
    $history_items[] = [
        'ID'      => get_the_ID(),
        'date'    => $post->post_title,
        'content' => $post->post_content,
        'url'     => get_post_meta(get_the_ID(), '연혁url', true),
    ];
endwhile;
?>

<script>
    (function () {
        var historyItems = <?= json_encode($history_items) ?>;
        var dateDelimiter = '<?= get_option('mytory_history_date_delimiter', '.') ?>';
        <?= file_get_contents(__DIR__ . '/../js/bundle.js') ?>
    })()
</script>
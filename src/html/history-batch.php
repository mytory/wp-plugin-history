<div class="wrap">
    <h1>연혁 일괄 입력</h1>

    <p>엑셀에서 입력한 뒤 복사해서 붙여넣기 하세요. 제목행은 복사하시면 안 됩니다.</p>
    <p>날짜로 내림차순 정렬해서 지금 입력돼 있는 것들 뒤에서부터 집어넣습니다.</p>
    <p>진행하기 전에 백업부터 하실 것을 권합니다.</p>

    <form method="post">
        <textarea class="large-text" required name="content" rows="30" placeholder="여기 입력하시면 됩니다."></textarea>
		<?php submit_button( '입력' ) ?>
    </form>
</div>
<div class="wrap">
    <h1>일괄 입력 결과</h1>
    <ul>
        <li>연혁 <?= number_format( $result['inserted_count'] ) ?>개를 입력했습니다.</li>
		<?php if ( $result['error_count'] > 0 ) { ?>
            <li>
                에러 <?= number_format( $result['error_count'] ) ?>개가 발생했습니다.
                <ul>
					<?php foreach ( $result['errors'] as $error ) { ?>
                        <li><code><?= $error['code'] ?></code>: <?= $error['message'] ?></li>
					<?php } ?>
                </ul>
            </li>
		<?php } ?>
    </ul>
</div>